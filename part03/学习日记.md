### 发布npm包
官网：https://www.npmjs.com/
第一运行：
```shell
npm adduser
```


第二运行：
```shell
npm publish
```

*默认源：
```shell
npm config set registry https://registry.npmjs.org
```



* 淘宝源：
```shell
npm config set registry https://registry.npm.taobao.org
```

token:c06654dc35d280d29c5709a093dfea168036f7a4
